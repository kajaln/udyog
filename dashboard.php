<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
 
    <title>Annasaheb Patil Arthik Magas Vikas Mahamandal Maryadit</title>
    <!-- Bootstrap CSS -->
    <link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Google Font CSS -->
    <link href="https://fonts.googleapis.com/css?family=Oxygen:400,700" rel="stylesheet"> 
   	<!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="css/responsive.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body ng-app="myApp">
	<header>
		<div class="container">
			header
		</div>
	</header> <!--Header Ends! -->

	<div class="main-container">
		<div class="breadcrumb">
			<div class="container">
				<ul>
					<li><strong>You are here: </strong></li>
					<li><a href="index.php">Home</a></li>
					<li><a href="#">Dashboard</a></li>
				</ul>
			</div>
		</div>
		<div class="container">
			<div class="container-menu">
				<ul>
					<li><a href="dashboard.php" class="btn btn-primary">Dashboard</a></li>
					<li>
						<div class="dropdown">
						  	<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Manage Application
						  	<span class="caret"></span></button>
						  	<ul class="dropdown-menu">
						    	<li><a href="#">Manage Project Application</a></li>
						    </ul>
						</div> 
					</li>
				</ul>
			</div>
			<div class="content">
				<h1>Dashboard</h1>

				<div class="box">
					<h2>Applied Application</h2>
					<table class="table table-bordered">
						<thead>
							<tr>
								<th scope="col">Application No.</th>
								<th scope="col">Date of Application</th>
								<th scope="col">Scheme Name</th>
								<th scope="col">Status</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><a href="view-applicaiton.php" target="_blank">20160302PAAP736</a></td>
								<td><span>02-03-2016</span></td>
								<td>Margin Money scheme </td>
								<td><span>Bank Approval</span></td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="box">
					<h2>Sanctioned Letter</h2>
					<table class="table table-bordered">
						<thead>
							<tr>
								<th scope="col">Application No.</th>
								<th scope="col">Applicant Name</th>
								<th scope="col">Sanction Date</th>
								<th scope="col">View Letter</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><a href="view-applicaiton.php" target="_blank">20160302PAAP736</a></td>
								<td><span>02-03-2016</span></td>
								<td>Margin Money scheme </td>
								<td><span>Bank Approval</span></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div> <!--Main Container Ends! -->
	<footer>
		<div class="container">
			footer
		</div>
	</footer> <!--Footer Ends! -->


  	<!-- jQuery -->
    <script src="js/jquery-1.11.1.js"></script>
    <!-- Bootstrap JS -->
    <script src="plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- Angular JS -->
    <script src="js/angular.min.js"></script>
    <!-- Controller JS-->
    <script src="js/app.js"></script>
</body>
</html>